package gld.tester;

import gld.algo.tlc.Key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ComputeBaselineDelays
{
    /**
     * Inner loop for CPT+SPSA optimizer executes a Boltzmann policy and this
     * class is a wrapper that takes in a policy parameter, runs the GLD
     * simulator and returns the delays for each traveler
     */
    GLDRunner gldRunnerObject;

    /**
     * Object that holds the delays for all travelers, organized path-wise
     */
    DelaysCollection delaysFromFixedTLC;

    int dimension;
    
    public int getDimension()
    {
        return dimension;
    }
    
    public ComputeBaselineDelays(String[] params, String mapdir, String map)
    {
        /**
         * Initialize GLDRunner with parameters (e.g. road network)
         * set TLC Id to Fixed TLC. This is used in computeBaselineDelays
         */
        String tlController = "SPSA";
        int tlControllerId = 0;
        int tlcCat = 0;
        gldRunnerObject = new GLDRunner(params, mapdir, map, tlController,
                tlcCat, tlControllerId);
        dimension = gldRunnerObject.init();
        
        delaysFromFixedTLC = new DelaysCollection();
    }
    
    /**
     * Run Fixed TLC and populate the baseline delays
     */
    public DelaysCollection runFixedTLCandComputeDelays()
    {
        System.out
                .println("############# Start: Fixed TLC run to obtain baseline delays ################");


        HashMap<Key, ArrayList<Double>> delaysPathWise = gldRunnerObject.run(null, 5000);
        delaysFromFixedTLC.setDelaysPathwise(delaysPathWise);        
        gldRunnerObject.resetSimulator();
        gldRunnerObject.finish();
     
        System.out
                .println("############# Finish: Fixed TLC run to obtain baseline delays ################");
        return delaysFromFixedTLC;
    }
    
}
