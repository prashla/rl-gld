package gld.tester;

import gld.algo.tlc.Key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DelaysCollection
{

    /**
     * HashMap that holds the delays for all travelers, organized path-wise
     */
    HashMap<Key, ArrayList<Double>> delaysPathWise;

    /**
     * HashMap that holds average delays from a Fixed TLC run (used as baseline
     * in calculating CPT value)
     */
    HashMap<Key, Double> avgDelayPathWise;
    
    double avgDelayOverall = 0.0;
    
    public HashMap<Key, ArrayList<Double>> getDelaysPathwise()
    {
        return delaysPathWise;
    }

    public HashMap<Key, Double> getAvgDelayPathwise()
    {
        return avgDelayPathWise;
    }
    
    public double getAvgOverallDelay()
    {
        return avgDelayOverall;
    }
    
    public void setDelaysPathwise(HashMap<Key, ArrayList<Double>> _dlyPthWise)
    {
        delaysPathWise = _dlyPthWise;
        calculateAvgDelaysPathwise();
    }

    
    void calculateAvgDelaysPathwise()
    {
        int nSamples = 0;
        avgDelayPathWise = new HashMap<Key, Double>();
        for (Key c : delaysPathWise.keySet())
        {
            ArrayList<Double> a = delaysPathWise.get(c);
            double sumDelays = 0;
            for (int i = 0; i < a.size(); i++)
            {
                sumDelays += (double) a.get(i);
            }
            nSamples += a.size();
            sumDelays = sumDelays / a.size();
             avgDelayPathWise.put(c, sumDelays);
        }

        // Print Avg path wise delays
        avgDelayOverall = 0.0;
        for (Key c : avgDelayPathWise.keySet())
        {
            Double a = avgDelayPathWise.get(c);
            avgDelayOverall += a;
        }
        avgDelayOverall = avgDelayOverall / avgDelayPathWise.size();
        System.out.printf("Average overall delay: %.2f from %d samples\n", avgDelayOverall, nSamples);
    }


//    public void printPathwiseDelays()
//    {
//        traceFile.println("Pathwise Delays:");
//        for (Key c : delaysPathWise.keySet())
//        {
//            traceFile.print(Arrays.toString(c.getVals()));
//            ArrayList<Double> a = delaysPathWise.get(c);
//            for (int i = 0; i < a.size(); i++)
//            {
//                traceFile.printf("%.2f,", (Double) a.get(i));
//            }
//            traceFile.println();
//        }
//    }

}
