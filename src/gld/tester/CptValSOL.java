package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import gld.*;
import gld.algo.tlc.Key;
import gld.algo.tlc.BoltzmannPolicyTLC;
import gld.infra.EdgeNode;
import gld.infra.Infrastructure;
import gld.infra.Junction;
import gld.infra.Node;
import gld.infra.SpecialNode;
import gld.sim.*;
import gld.sim.stats.*;
import gld.utils.ListCombinationGenerator;

/**
 * CPT policy optimizing outer loop that is based on SPSA
 * 
 * @see CptRunner
 * @version 0.1 11 Feb 2016
 * @author Prashanth L.A.
 */
public class CptValSOL extends SpsaOuterLoop
{
    CptValEstimator cptValEst;

    CptValSOL(String[] params, String startTimeStr, String mapdir, String map,
            int numSimulationsForPolicyTuning,
            int simulatedMDPTrajectoryLengthForTraining)
    {
        super(params, mapdir, map, numSimulationsForPolicyTuning,
                simulatedMDPTrajectoryLengthForTraining);

        // open file handles for trace, theta and results log
        this.initFiles(startTimeStr, map);


        cptValEst = new CptValEstimator(delaysFromFixedTLC);
    }

    /**
     * Open files for trace prints, theta in each iteration and results
     * (CPT-value) from testing phase
     * 
     * @param netw
     *            Specify the path to the map file containing the road network
     *            to be tested
     */
    public void initFiles(String startTimeStr, String netw)
    {
        try
        {
            // policy parameter log
            thetaFile = new PrintWriter(new FileWriter(new File(startTimeStr
                    + "//" + "CptVal_SPSA_theta_" + netw + ".txt")));
            thetaFile.println("TLC Algo: " + "CPT-value SPSA");
            thetaFile.println("# theta[1] .... theta[dimFeatures]");
            thetaFile.println("#");
            // trace log
            traceFile = new PrintWriter(new FileWriter(new File(startTimeStr
                    + "//" + "CptVal_SPSA_trace_" + netw + ".log")));

            traceFile
                    .println("-----------------------------------------------");
            traceFile.println("Network: " + netw);
            traceFile.println("numIterationsForPolicyTuning: "
                    + this.numSimulationsForPolicyTuning);
            traceFile.println("simulatedMDPTrajectoryLengthForTraining: "
                    + this.simulatedMDPTrajectoryLengthForTraining);
            traceFile.println("theta_min: " + THETA_MIN + " theta_max: "
                    + THETA_MAX + " theta_init:" + THETA_INIT);
            traceFile
                    .println("-----------------------------------------------");

        }
        catch (Exception e)
        {
            System.out.println("CPT_SPSA failed to initialise.");
        }
    }

    /**
     * Close file handles
     */
    public void finish()
    {
        thetaFile.close();
        traceFile.close();
    }

    /**
     * This constitutes the training phase where the policy parameter is tuned
     * to maximize CPT-value
     * 
     * @param weightType
     *            CPT or AVG
     */
    double[] optimizepolicy()
    {
        traceFile
                .println("############# Optimizing CPT value using SPSA ################");
        System.out
                .println("############# Optimizing CPT value using SPSA ################");

        double thetaGood[] = {0.2,0.2,0.6,0.7,0.6,0.2,0.3,0.8};
        for (int i = 0; i < theta.length; i++)
        {
            theta[i] = thetaGood[i];
        }

        // Set to Boltzmann policy runner
        int tlControllerId = 0;
        int tlcCat = 1;
        gldRunner.setTLC(tlcCat, tlControllerId);

        for (int simNr = 1; simNr < numSimulationsForPolicyTuning; simNr++)
        {
            traceFile.println("############################ " + simNr
                    + " ################################");
            System.out.println("############################ " + simNr
                    + " ################################");
            boolean positivePerturbedSimulation = false;
            if ((simNr % 2) == 0)
            {
                positivePerturbedSimulation = true;
            }
            double thetaSim[];
            thetaSim = getTheta(positivePerturbedSimulation);

            HashMap<Key, ArrayList<Double>> delays = gldRunner.run(thetaSim,
                    simulatedMDPTrajectoryLengthForTraining);
            delaysPathWise.setDelaysPathwise(delays);
            gldRunner.resetSimulator();

            if ((simNr % 2) == 0)
            {
                valPlus = cptValEst.getEstimate(delaysPathWise);
            }
            else if ((simNr % 2) == 1)
            {
                valMinus = cptValEst.getEstimate(delaysPathWise);
                printCptValOfTheta();
                updateParams(simNr);
            }
        }
        return theta;
    }    
}
