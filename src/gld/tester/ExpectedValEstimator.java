package gld.tester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import gld.algo.tlc.Key;

public class ExpectedValEstimator extends Estimator
{
    public ExpectedValEstimator()
    {
        super();
    }

    /**
     * Estimate the AVG-value for one path in the network using the sample
     * delays in the Hashmap
     * 
     * @param darr
     *            samples using which Expected-value is estimated
     */
    public double getEstimateForOnePath(ArrayList<Double> darr)
    {
        double darrSum = 0.0;
        for (int i = 0; i < darr.size(); i++)
        {
            double darrEntry = darr.get(i);
            darrSum += darrEntry;
        }
        double darrAvg = darrSum / (double) darr.size();

        return darrAvg;
    }

    /**
     * Estimate the CPT-value using a quantile-based approach
     * 
     * @param weightType
     *            if CPT, uses both utilities and distortion via weights in the
     *            CPT-value, else just sample means (for AVG option)
     */
    double getEstimate(DelaysCollection delaysPathWise)
    {
        int totalSamples = 0;

        for (Key c : delaysPathWise.getDelaysPathwise().keySet())
        {
            ArrayList<Double> darr = delaysPathWise.getDelaysPathwise().get(c);

            weightsPathWise.put(c, (double) darr.size());
            totalSamples += darr.size();

            double valCurrPath = 0.0;
            valCurrPath = getEstimateForOnePath(darr);
            valsPathWise.put(c, (Double) valCurrPath);
//            System.out.print(Arrays.toString(c.getVals()));
//            System.out.printf("%.2f from %d samples\n", valCurrPath,
//                    darr.size());
        }

        double expectedValOverall = 0.0;
        for (Key c : valsPathWise.keySet())
        {
            double weightForThisPath = weightsPathWise.get(c)
                    / (double) totalSamples;
            weightsPathWise.put(c, weightForThisPath);
            double currVal = weightsPathWise.get(c) * valsPathWise.get(c);
            expectedValOverall += currVal;
        }

//        System.out.println("AVG Val: " + expectedValOverall);

        return expectedValOverall;
    }

}
