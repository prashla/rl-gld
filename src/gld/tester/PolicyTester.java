package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import gld.algo.tlc.Key;

public class PolicyTester
{
    /**
     * Inner loop for CPT+SPSA optimizer executes a Boltzmann policy and this
     * class is a wrapper that takes in a policy parameter, runs the GLD
     * simulator and returns the delays for each traveler
     */
    GLDRunner gldRunner;

    /**
     * File handles corresponding to files that record the policy parameter,
     * traces and final test results.
     */
    PrintWriter resultsFile, allDelaysFile;

    /**
     * After the policy is tuned using SPSA, the policy parameter is fixed and
     * then a number of independent simulations (that this variable is set to)
     * are run and the CPT-value from each simulation is recorded
     */
    int numIterationsForTesting;
    /**
     * length of each simulated trajectory for testing phase
     */
    int simulatedMDPTrajectoryLengthForTesting;

    /**
     * Object that holds the delays for all travelers, organized path-wise
     */
    DelaysCollection delaysColln;

    ComputeBaselineDelays baselineDelays;
    HashMap<Key, Double> avgDelaysFromFixedTLC;
    CptValEstimator cptValEst;
    ExpectedValEstimator expectedValEst;

    private double[] thetaForTesting;

    /**
     * Initialize the CPT+SPSA algorithm
     * 
     * @param mapdir
     *            +map Specify the path to the map file containing the road
     *            network to be tested
     * @param numIterationsForPolicyTuning
     *            the number of iterations of the SPSA outer loop
     * @param numIterationsForTesting
     *            the number of iterations for testing the policy output by SPSA
     *            outer loop (after numIterationsForPolicyTuning)
     * @param numSamplesForEstimation
     *            number of simulated trajectories for CPT-value estimation in
     *            the inner loop
     * @param simulatedMDPTrajectoryLength
     *            length of simulated MDP trajectory for any given policy
     *            parameter
     * @param weightType
     *            AVG = plain sample means, no utilities, no distortions via
     *            weights CPT = uses utilities and distortion via weights
     */
    public PolicyTester(String[] params, String startTimeStr, String mapdir,
            String map, int _numIterTest, int _trajLength, double _theta[])
    {
        /**
         * Compute baseline delays by running Fixed TLC
         */
        baselineDelays = new ComputeBaselineDelays(params, mapdir, map);
        DelaysCollection delaysFromFixedTLC = baselineDelays
                .runFixedTLCandComputeDelays();

        cptValEst = new CptValEstimator(delaysFromFixedTLC);
        expectedValEst = new ExpectedValEstimator();

        thetaForTesting = new double[_theta.length];
        for (int i = 0; i < thetaForTesting.length; i++)
        {
            thetaForTesting[i] = _theta[i];
        }

        numIterationsForTesting = _numIterTest;
        simulatedMDPTrajectoryLengthForTesting = _trajLength;

        // results log
        try
        {
            resultsFile = new PrintWriter(new FileWriter(new File(startTimeStr
                    + "//"  + "results_" + map + ".txt")));
            allDelaysFile = new PrintWriter(new FileWriter(new File(startTimeStr
                    + "//"  +"allDelays_" + map + ".txt")));
        }
        catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("numIterationsForTesting: "
                + this.numIterationsForTesting);
        System.out.println("simulatedMDPTrajectoryLengthForTesting: "
                + this.simulatedMDPTrajectoryLengthForTesting);

        /**
         * Initialize GLDRunner with parameters (e.g. road network) set TLC Id
         * to Fixed TLC. This is used in computeBaselineDelays
         */
        String tlController = "SPSA";
        int tlControllerId = 0;
        int tlcCat = 1;
        gldRunner = new GLDRunner(params, mapdir, map, tlController, tlcCat,
                tlControllerId);
        gldRunner.init();

        delaysColln = new DelaysCollection();
    }

    /**
     * This constitutes the test phase where the policy parameter is fixed and
     * multiple simulations are performed and the histogram of resulting
     * CPT-values is analyzed
     * 
     * @param weightType
     *            CPT or AVG
     */
    void testPolicy()
    {
        resultsFile.println("############# Testing policy ################");
        System.out.println("############# Testing policy ################");

        resultsFile.println("## theta for testing: [");
        for (int i = 0; i < thetaForTesting.length; i++)
        {
            resultsFile.print(thetaForTesting[i] + ",");
        }
        resultsFile.println("]");

        double[] cptValArray = new double[numIterationsForTesting];
        double[] avgValArray = new double[numIterationsForTesting];
        for (int simNr = 0; simNr < numIterationsForTesting; simNr++)
        {
            System.out.println("############################ " + simNr
                    + " ################################");
            resultsFile.println("############################ " + simNr
                    + " ################################");

            // Run simulation and collect delays
            HashMap<Key, ArrayList<Double>> delays = gldRunner.run(
                    thetaForTesting, simulatedMDPTrajectoryLengthForTesting);
            delaysColln.setDelaysPathwise(delays);
            gldRunner.resetSimulator();

            // Print all delay samples pathwise into allDelaysFile
            allDelaysFile.println("Pathwise Delays:");
            for (Key c : delaysColln.getDelaysPathwise().keySet())
            {
                allDelaysFile.print(Arrays.toString(c.getVals()));
                ArrayList<Double> a = delaysColln.getDelaysPathwise().get(c);
                for (int i = 0; i < a.size(); i++)
                {
                    allDelaysFile.printf("%.2f,", (Double) a.get(i));
                }
                allDelaysFile.println();

            }
            
            // Estimation of CPT *note: has baseline*
            cptValArray[simNr] = cptValEst.getEstimate(delaysColln);

            // Estimation of expectation *note: no baseline*
            avgValArray[simNr] = expectedValEst.getEstimate(delaysColln);

            // Print CPT/AVG vals pathwise
            resultsFile
                    .println("################################################################\n Path,PathWeight,Avg-val,PathWeight,Cpt-val");
            for (Key c : cptValEst.getValsPathwise().keySet())
            {
                resultsFile.print(Arrays.toString(c.getVals())+ "\t");
                Double avgWeightForThisPath = expectedValEst.getWeightsPathwise().get(c);
                Double avgValForThisPath = expectedValEst.getValsPathwise().get(c);
                Double cptWeightForThisPath = cptValEst.getWeightsPathwise().get(c);
                Double cptValForThisPath = cptValEst.getValsPathwise().get(c);
                resultsFile.printf("%.2f,%.2f,%.2f,%.2f", avgWeightForThisPath,avgValForThisPath,cptWeightForThisPath,cptValForThisPath);
                resultsFile.println();
            }
        }

        resultsFile.println("########## AVG-value CPT-value ##############");
        for (int i = 0; i < cptValArray.length; i++)
        {
            // Record theta
            resultsFile.printf("%.2f\t%.2f\n", avgValArray[i], cptValArray[i]);
            System.out.printf("%.2f\t%.2f\n", avgValArray[i], cptValArray[i]);
        }
        resultsFile.close();
        allDelaysFile.close();
    }
}
