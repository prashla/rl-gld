package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Random;

import gld.*;

/**
 * Wrapper for running the CPT policy optimizer
 * 
 * @see CptSPSAOuterLoop
 * @version 0.1 11 Feb 2016
 * @author Prashanth L.A.
 */
public class ExpectationRunner
{
    public static void main(String[] params)
    {
        /**
         * the number of iterations of the SPSA outer loop - each iteration
         * invovles 2 simulated trajectories
         */
        int numSimulationsForPolicyTuning = 1000;
        /**
         * After the policy is tuned using SPSA, the policy parameter is fixed
         * and then a number of independent simulations (that this variable is
         * set to) are run and the CPT-value from each simulation is recorded
         */
        int numIterationsForTesting = 1;
        /**
         * length of each simulated trajectory
         */
        int simulatedMDPTrajectoryLengthForTraining = 2000;
        /**
         * length of each simulated trajectory
         */
        int simulatedMDPTrajectoryLengthForTesting = 10000;

        /**
         * Create a new directory with timestamp for its name
         */
        Calendar calendar1 = new GregorianCalendar();
        String startTimeStr, am_pm;
        int hour = calendar1.get(Calendar.HOUR);
        int minute = calendar1.get(Calendar.MINUTE);
        int second = calendar1.get(Calendar.SECOND);
        if (calendar1.get(Calendar.AM_PM) == 0)
            am_pm = "AM";
        else
            am_pm = "PM";
        startTimeStr = calendar1.get(Calendar.YEAR) + "_"
                + calendar1.get(Calendar.MONTH) + "_"
                + calendar1.get(Calendar.DATE) + "_" + am_pm + "_" + hour + "_"
                + minute + "_" + second;
        System.out.println("Current Time : " + hour + ":" + minute + ":"
                + second + " " + am_pm);
        (new File(startTimeStr)).mkdir();

        String mapdir = "/home/prashanth/Documents/bitbucket/rl-gld/maps/";
        String map = "simpler";

        // CPT optimizer
        ExpectedValSOL expectedValSol = new ExpectedValSOL(params, startTimeStr, mapdir, map,
                numSimulationsForPolicyTuning,
                simulatedMDPTrajectoryLengthForTraining);

        double[] thetaCpt = expectedValSol.optimizepolicy();

        // close all file handles
        expectedValSol.finish();

        // Test the policy at the end of training phase and log the CPT and AVG
        // values for each replication
        PolicyTester policyTester = new PolicyTester(params, startTimeStr, mapdir, map,
                numIterationsForTesting,
                simulatedMDPTrajectoryLengthForTesting, thetaCpt);
        policyTester.testPolicy();
    }
}
