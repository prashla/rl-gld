package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import gld.*;
import gld.algo.tlc.Key;
import gld.algo.tlc.BoltzmannPolicyTLC;
import gld.infra.EdgeNode;
import gld.infra.Infrastructure;
import gld.infra.Junction;
import gld.infra.Node;
import gld.infra.SpecialNode;
import gld.sim.*;
import gld.sim.stats.*;
import gld.utils.ListCombinationGenerator;

/**
 * CPT policy optimizing outer loop that is based on SPSA
 * 
 * @see CptRunner
 * @version 0.1 11 Feb 2016
 * @author Prashanth L.A.
 */
public abstract class SpsaOuterLoop 
{
    /**
     * Inner loop for CPT+SPSA optimizer executes a Boltzmann policy and this
     * class is a wrapper that takes in a policy parameter, runs the GLD
     * simulator and returns the delays for each traveler
     */
    GLDRunner gldRunner;

    /**
     * dimension of state-action features -> used in Boltzmann policy, same as
     * policy parameter (theta) dimension
     */
    int dimension;

    /**
     * the number of iterations of the SPSA outer loop - each iteration invovles
     * 2 simulated trajectories
     */
    int numSimulationsForPolicyTuning;
    /**
     * length of each simulated trajectory for training phase
     */
    int simulatedMDPTrajectoryLengthForTraining;

    /**
     * File handles corresponding to files that record the policy parameter,
     * traces and final test results.
     */
    PrintWriter thetaFile, traceFile;

    /**
     * For stability, each co-ordinate of theta is forced to stay within
     * THETA_MIN and THETA_MAX
     */
    double THETA_MIN = 0.1, THETA_MAX = 1.0, THETA_INIT = 0.5;

    Random randGen;

    /**
     * Object that holds the delays for all travelers, organized path-wise
     */
    DelaysCollection delaysPathWise;
    
    /**
     * Fixed TLC delays
     */
    ComputeBaselineDelays baselineDelays;
    HashMap<Key, Double> avgDelaysFromFixedTLC;
    DelaysCollection delaysFromFixedTLC;
    
    /**
     * Step-size for gradient descent update of theta
     */
    double an = 1.0;

    /**
     * Perturbation constants used in SPSA based gradient estimation
     */
    double deltan;
    /**
     * Perturbation random variables used in SPSA based gradient estimation.
     * These follow Rademacher distribution
     */
    int Delta[];

    /**
     * Policy parameter (unperturbed) and perturbed policy parameter
     */
    double theta[], thetaPerturbed[]; // actor policy weights

    /**
     * valPlus -> value (CPT/expectation) estimate from trajectory 1
     * valMinus -> value (CPT/expectation) estimate from trajectory 2
     */
    double valPlus, valMinus;
    

    /**
     * Initialize the CPT+SPSA algorithm
     * 
     * @param mapdir
     *            +map Specify the path to the map file containing the road
     *            network to be tested
     * @param numIterationsForPolicyTuning
     *            the number of iterations of the SPSA outer loop
     * @param simulatedMDPTrajectoryLength
     *            length of simulated MDP trajectory for any given policy
     *            parameter
     * @param weightType
     *            AVG = plain sample means, no utilities, no distortions via
     *            weights CPT = uses utilities and distortion via weights
     */
    SpsaOuterLoop(String[] params, String mapdir, String map,
            int numSimulationsForPolicyTuning, 
            int simulatedMDPTrajectoryLengthForTraining)
    {
        this.numSimulationsForPolicyTuning = numSimulationsForPolicyTuning;
        this.simulatedMDPTrajectoryLengthForTraining = simulatedMDPTrajectoryLengthForTraining;

        System.out.println("-----------------------------------------------");
        System.out.println("Network: " + map);
        System.out.println("numIterationsForPolicyTuning: "
                + this.numSimulationsForPolicyTuning);
        System.out.println("simulatedMDPTrajectoryLengthForTraining: "
                + this.simulatedMDPTrajectoryLengthForTraining);
        System.out.println("-----------------------------------------------");

        // set TLC Id to Fixed TLC. This is used in computeBaselineDelays
        String tlController = "SPSA";
        int tlControllerId = 0;
        int tlcCat = 1;
        gldRunner = new GLDRunner(params, mapdir, map, tlController,
                tlcCat, tlControllerId);
        dimension = gldRunner.init();

        // Instantiate perturbation and parameters
        Delta = new int[dimension];
        theta = new double[dimension];
        thetaPerturbed = new double[dimension];

        // This is the recommended setting from Spall's code 
        deltan = 1.9;

        randGen = new Random(1015);
        delaysPathWise = new DelaysCollection();
        
        /**
         * Compute baseline delays by running Fixed TLC
         */
        baselineDelays = new ComputeBaselineDelays(params, mapdir, map);
        delaysFromFixedTLC = baselineDelays
                .runFixedTLCandComputeDelays();
    }


    /**
     * Return a perturbed policy parameter
     * 
     * @param positivePerturbedSimulation
     *            if true, returns (theta+deltaDelta) else (theta-deltaDelta)
     */
    double[] getTheta(boolean positivePerturbedSimulation)
    {
        for (int i = 0; i < Delta.length; i++)
        {
            int rno = randGen.nextInt(2);
            Delta[i] = rno;
            if (rno == 0)
            {
                Delta[i] = -1;
            }
            if (positivePerturbedSimulation == true)
            {
                thetaPerturbed[i] = theta[i] + deltan * Delta[i];
            }
            else
            {
                thetaPerturbed[i] = theta[i] - deltan * Delta[i];
            }
        }
        if (positivePerturbedSimulation == true)
        {
            traceFile
                    .println("************** Positive perturbed simulation ************************");
        }
        else
        {
            traceFile
                    .println("************** Negative perturbed simulation ************************");
        }
        traceFile.println("Delta: " + Arrays.toString(Delta));
        System.out.println("Delta: " + Arrays.toString(Delta));
        traceFile.printf("delta: %.2f\n", deltan);
        System.out.printf("delta:  %.2f\n", deltan);
        traceFile.printf("theta: [");
        System.out.printf("theta: [");
        for (int i = 0; i < theta.length; i++)
        {
            traceFile.printf("%.1f,", theta[i]);
            System.out.printf("%.1f,", theta[i]);
        }
        traceFile.println("]");
        System.out.println("]");
        traceFile.printf("thetaPerturbed: [");
        System.out.printf("thetaPerturbed: [");
        for (int i = 0; i < thetaPerturbed.length; i++)
        {
            traceFile.printf("%.1f,", thetaPerturbed[i]);
            System.out.printf("%.1f,", thetaPerturbed[i]);
        }
        traceFile.println("]");
        System.out.println("]");

        return thetaPerturbed;
    }


    /**
     * Update policy parameter theta in descent direction using SPSA based
     * gradient estimates derived out of CPT-value estimates for the perturbed
     * simulations
     * 
     * @param simNr
     *            SPSA based outer loop iteration index
     */
    void updateParams(int simNr)
    {
        double updateFactor = (valPlus - valMinus) / (2 * deltan);
        traceFile.printf("updateFactor: %.2f\n", updateFactor);
        for (int i = 0; i < theta.length; i++)
        {
            theta[i] += an * updateFactor / Delta[i];
            if (theta[i] <= THETA_MIN)
                theta[i] = THETA_MIN;
            if (theta[i] >= THETA_MAX)
                theta[i] = THETA_MAX;
        }

        double iterationNum = Math.ceil(simNr*0.5);
        String ln = iterationNum + "\t";
        thetaFile.print(ln);
        for (int i = 0; i < theta.length; i++)
        {
            // Record theta
            thetaFile.printf("%f\t", theta[i]);
        }
        thetaFile.println();

        double stepsizeDenom = iterationNum + 50;
        an = Math.pow(stepsizeDenom, -1);
        deltan = Math.pow(iterationNum, -0.101) * 1.9;
    }
    
    void printCptValOfTheta()
    {
        HashMap<Key, ArrayList<Double>> delays = gldRunner.run(theta,
                simulatedMDPTrajectoryLengthForTraining);
        DelaysCollection delaysPathWiseTemp = new DelaysCollection();
        delaysPathWiseTemp.setDelaysPathwise(delays);
        gldRunner.resetSimulator();

        ExpectedValEstimator expValEstTemp = new ExpectedValEstimator();
        CptValEstimator cptValEstTemp = new CptValEstimator(delaysFromFixedTLC);
        double avgval = expValEstTemp.getEstimate(delaysPathWiseTemp);
        double cptval = cptValEstTemp.getEstimate(delaysPathWiseTemp);
        
        System.out.println("AVGval theta (unperturbed):" + avgval + "CPTval theta (unperturbed):" + cptval);
    }
}
