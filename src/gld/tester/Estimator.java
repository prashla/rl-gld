package gld.tester;

import gld.algo.tlc.Key;

import java.util.HashMap;

public class Estimator
{
    HashMap<Key, Double> valsPathWise;
    HashMap<Key, Double> weightsPathWise;

    
    public Estimator()
    {
        valsPathWise = new HashMap<Key, Double>();
        weightsPathWise = new HashMap<Key, Double>();
    }
    
    public HashMap<Key, Double> getValsPathwise()
    {
        return valsPathWise;
    }
    
    public HashMap<Key, Double> getWeightsPathwise()
    {
        return weightsPathWise;
    }

}
