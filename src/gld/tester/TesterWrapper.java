package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import gld.*;

/**
 * Wrapper for running the CPT policy optimizer
 * 
 * @see CptSPSAOuterLoop
 * @version 0.1 11 Feb 2016
 * @author Prashanth L.A.
 */
public class TesterWrapper
{
    public static void main(String[] params)
    {
        /**
         * After the policy is tuned using SPSA, the policy parameter is fixed
         * and then a number of independent simulations (that this variable is
         * set to) are run and the CPT-value from each simulation is recorded
         */
        int numIterationsForTesting = 10;
        /**
         * length of each simulated trajectory
         */
        int simulatedMDPTrajectoryLengthForTesting = 10000;

        String mapdir = "/home/prashanth/Documents/bitbucket/rl-gld/maps/";
        String map = "simpler";

        // Test the policy at the end of training phase and log the CPT and AVG
        // values for each replication
//        double thetaCpt[] = { 0.1,0.1,0.8754375139845739,0.9480348682718259,0.1,0.25218361116667065,0.6100028344095202,0.9940458208670252};
        double thetaAvg[] = { 0.1,0.1,0.8112413157952174,0.988742226418233,0.14192422265562776,0.16187230252125853,0.7660320918938037,0.9638450714245461};
        PolicyTester policyTester = new PolicyTester(params, "testFixedTheta", mapdir, map,
                numIterationsForTesting,
                simulatedMDPTrajectoryLengthForTesting, thetaAvg);
        policyTester.testPolicy();

    }
}
