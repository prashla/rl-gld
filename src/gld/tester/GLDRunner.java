package gld.tester;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import gld.*;
import gld.algo.tlc.Key;
import gld.infra.EdgeNode;
import gld.infra.Infrastructure;
import gld.infra.Junction;
import gld.infra.Node;
import gld.infra.SpecialNode;
import gld.sim.*;
import gld.sim.stats.*;
import gld.utils.ListCombinationGenerator;

public class GLDRunner
{
    String tlController, mapdir, map;
    public int tlcID, tlcCat;
    String[] params;

    GLDStarter gldStarter;
    SimController simController;
    SpecialNode[] specialnodes;
    TotalWaitTrackingView viewATWT;
    TotalRoadusersTrackingView viewTAR;

    GLDRunner(String[] params, String mapdir, String map, String tlController,
            int tlcCat, int tlcID)
    {
        this.params = params;
        this.mapdir = mapdir;
        this.map = map;
        this.tlController = tlController;
        this.tlcCat = tlcCat;
        this.tlcID = tlcID;

        gldStarter = new GLDStarter(params, GLDStarter.SIMULATOR);
        simController = (SimController) gldStarter.getController();
    }

    public void setTLC(int tlcCat, int tlcID)
    {
        this.tlcCat = tlcCat;
        this.tlcID = tlcID;
    }

    public int init()
    {
        simController.setTLC(tlcCat, tlcID);

        try
        {
            simController.doLoad(mapdir + map + ".sim");
        }
        catch (Exception e)
        {
        }

        Infrastructure infra = simController.getSimModel().getInfrastructure();
        Node[] nodes;
        int numRealSigns = 0;
        nodes = infra.getAllNodes();
        for (int i = 0; i < nodes.length; i++)
        {
            numRealSigns += nodes[i].getNumSigns();
        }

        infra.setTitle(mapdir + map + ".sim");
        SpecialNode[] specialnodes = infra.getSpecialNodes();

        viewATWT = new TotalWaitTrackingView(simController.getSimModel()
                .getCurCycle(), simController.getSimModel());
        TrackerFactory.genExtTracker(simController.getSimModel(),
                simController, viewATWT);
        viewTAR = new TotalRoadusersTrackingView(simController.getSimModel()
                .getCurCycle(), simController.getSimModel());
        TrackerFactory.genExtTracker(simController.getSimModel(),
                simController, viewTAR);

        return numRealSigns;
    }

    public void resetSimulator()
    {
        try
        {
            simController.getSimModel().reset();
        }
        catch (SimulationRunningException e)
        {
            e.printStackTrace();
        }
    }

    public HashMap<Key, ArrayList<Double>> run(double[] theta, int simDuration)
    {
        simController.setSpeed(3); // speed to maximum
        simController.setTLC(tlcCat, tlcID); // RS_SPSA

        simController.getSimModel().setSimName("CPT SPSA");
        simController.getSimModel().getTLController().setThresholds(6, 14, 130);

        simController.getSimModel().getTLController()
                .setLanesFile(mapdir + map + "_pri.txt");
        simController.getSimModel().getTLController().setTheta(theta);
        simController.getSimModel().getTLController().clearOldDelays();
        
        simController.unpause();
        simController.getSimModel().setSimulationDuration(simDuration);

        while (simController.getSimModel().getCurCycle() < simController
                .getSimModel().getSimulationDuration())
        {
            try
            {
                Thread.sleep(5000);
                System.out.println(
                        "CYCLE:" + simController.getSimModel().getCurCycle());
            }
            catch (InterruptedException e)
            {

            }
        }
        
        HashMap<Key, ArrayList<Double>> dly = simController.getSimModel().getTLController()
                .getPathwiseDelays();
        simController.getSimModel().pause();
        try
        {
            simController.getSimModel().reset();
        }
        catch (SimulationRunningException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return dly;
    }
    
    public void finish()
    {
        simController.getSimModel().stop();
    }
}
