package gld.tester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import gld.algo.tlc.Key;

public class CptValEstimator extends Estimator
{
    private static final int CPT_PLUS = 3, CPT_MINUS = 4;
    private static final double UTIL_EXPONENT = 0.88;
//    private static final double CPT_EXPONENT_PLUS = 0.4, CPT_EXPONENT_MINUS = 0.4;
    private static final double CPT_EXPONENT_PLUS = 0.61, CPT_EXPONENT_MINUS = 0.69;
    DelaysCollection delaysFromFixedTLC;

    public CptValEstimator(DelaysCollection _delaysFixedTLC)
    {
        super();
        delaysFromFixedTLC = _delaysFixedTLC;
    }

    /**
     * The weight function that distorts probabilities
     * 
     * @param x
     *            the probability that needs to be distorted
     * @param weightType
     *            if EUT, no distortion, else use the Tversky-Kahnemann
     *            suggested weight function with two different parameters, based
     *            on whether the distortion is for the first or second integral
     *            of CPT-value
     */
    double weight(double x, int cptWeightPart)
    {
        double cptWeight = 0;
        switch (cptWeightPart)
        {
        case CPT_PLUS:
            double denom = Math.pow(x, CPT_EXPONENT_PLUS) + Math.pow(1 - x, CPT_EXPONENT_PLUS);
            cptWeight = Math.pow(x, CPT_EXPONENT_PLUS) / Math.pow(denom, Math.pow(CPT_EXPONENT_PLUS, -1));
            // traceFile.printf("x:%.2f cpt+:%.2f,",x, cptWeight);
            break;
        case CPT_MINUS:
            double denom2 = Math.pow(x, CPT_EXPONENT_MINUS) + Math.pow(1 - x, CPT_EXPONENT_MINUS);
            cptWeight = Math.pow(x, CPT_EXPONENT_MINUS)
                    / Math.pow(denom2, Math.pow(CPT_EXPONENT_MINUS, -1));
            // traceFile.printf("x:%.2f cpt+:%.2f,",x, cptWeight);
            break;
        }

        return cptWeight;
    }

    /**
     * Estimate the CPT-value for one path in the network using the sample
     * delays in the Hashmap
     * 
     * @param darr
     *            samples using which CPT-value is estimated
     */
    public double getEstimateForOnePath(ArrayList<Double> darr)
    {
        Collections.sort(darr);

        ArrayList<Double> darrPlus = new ArrayList<Double>();
        ArrayList<Double> darrMinus = new ArrayList<Double>();

        for (int i = 0; i < darr.size(); i++)
        {
            double darrEntry = darr.get(i);
            if (darrEntry < 0)
            {
                darrMinus.add(darrEntry);
            }
            else
            {
                darrPlus.add(darrEntry);
            }
        }

        double cptValPlus = 0.0, cptValMinus = 0.0;
        int nLocal = darrPlus.size();
        for (int i = 1; i <= nLocal; i++)
        {
            cptValPlus += Math.pow(darrPlus.get(i - 1), UTIL_EXPONENT)
                    * (weight((double) (nLocal + 1 - i) / nLocal, CPT_PLUS)
                            - weight((double) (nLocal - i) / nLocal, CPT_PLUS));
        }

        nLocal = darrMinus.size();
        for (int i = 1; i <= nLocal; i++)
        {
            cptValMinus += 2.25 * Math.pow(Math.abs(darrMinus.get(i - 1)), UTIL_EXPONENT)
                    * (weight((double) i / nLocal, CPT_MINUS)
                            - weight((double) (i - 1) / nLocal, CPT_MINUS));
        }

        double cptVal = cptValPlus - cptValMinus;

        return cptVal;
    }

    /**
     * Estimate the CPT-value using a quantile-based approach
     * 
     * @param delays pathwise delays
     */
    double getEstimate(DelaysCollection delaysPathWise)
    {
        int totalSamples = 0;

        for (Key currKey : delaysPathWise.getDelaysPathwise().keySet())
        {
            ArrayList<Double> darr = delaysPathWise.getDelaysPathwise().get(currKey);

            weightsPathWise.put(currKey, (double)darr.size());
            totalSamples += darr.size();

            double cptValCurrPath = 0.0;
            double avgDelayVal;
            if (delaysFromFixedTLC.getAvgDelayPathwise().get(currKey) == null)
            {
                // this number is the average delay from the samples for
                // Fixed priority TLC
                avgDelayVal = delaysPathWise.avgDelayOverall;
            }
            else
            {
                avgDelayVal = delaysFromFixedTLC.getAvgDelayPathwise()
                        .get(currKey);
            }

            for (int i = 0; i < darr.size(); i++)
            {
                Double dval = avgDelayVal - darr.get(i);
                darr.set(i, dval);
            }
            cptValCurrPath = getEstimateForOnePath(darr);
            valsPathWise.put(currKey, (Double) cptValCurrPath);
//            System.out.print(Arrays.toString(currKey.getVals()));
//            System.out.printf("%.2f from %d samples\n", cptValCurrPath, darr.size());
        }

        double cptValOverall = 0.0;
        for (Key c : valsPathWise.keySet())
        {
            double weightForThisPath = weightsPathWise.get(c)
                    / (double) totalSamples;
            weightsPathWise.put(c, weightForThisPath);
            double currCPTval = weightsPathWise.get(c)
                    * valsPathWise.get(c);
            cptValOverall += currCPTval;
        }

//        System.out.println("CPT Val: " + cptValOverall);

        return cptValOverall;
    }

}
