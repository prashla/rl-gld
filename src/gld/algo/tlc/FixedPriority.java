/*-----------------------------------------------------------------------
 * Copyright (C) 2001 Green Light District Team, Utrecht University 
 *
 * This program (Green Light District) is free software.
 * You may redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by
 * the Free Software Foundation (version 2 or later).
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * See the documentation of Green Light District for further information.
 *------------------------------------------------------------------------*/

package gld.algo.tlc;

import gld.*;
import gld.sim.*;
import gld.algo.tlc.*;
import gld.infra.*;
import gld.utils.*;
import gld.xml.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * This controller will switch TrafficLights so that the Trafficlight with the
 * longest queue of waiting Roadusers will get set to green.
 * 
 * @author Group Algorithms
 * @version 1.0
 */
public class FixedPriority extends TLController
{
    public final static String shortXMLName = "fixed-priority";

    // TLC vars
    protected Infrastructure infrastructure;
    protected Node[] nodes;
    protected TrafficLight[][] tls;
    protected Node[] allnodes;

    // HashMap<Key, ArrayList<Integer>> delaysPathWiseLQ;

    Junction[] junctions;

    protected int num_nodes;
    int switchPeriod = 10;
    int periodIndex;

    public int getSwitchPeriod()
    {
        return switchPeriod;
    }

    public void setSwitchPeriod(int k)
    {
        switchPeriod = k;
    }

    /**
     * The constructor for TL controllers
     * 
     * @param The
     *            model being used.
     */
    public FixedPriority(Infrastructure i)
    {
        super(i);
    }

    public void setInfrastructure(Infrastructure infra)
    {
        super.setInfrastructure(infra);
        periodIndex = 0;
        junctions = infra.getJunctions();
        nodes = infra.getAllNodes();
        num_nodes = infra.getAllNodes().length;
        for (int i = 0; i < junctions.length; i++)
        {
            junctions[i].setConfigIndex(0);
        }
        // delaysPathWiseLQ = new HashMap<Key, ArrayList<Integer>>();
        delaysPathWise = new HashMap<Key, ArrayList<Double>>();
    }

    public void updateCost()
    {
        int num_lanes;

        // get current state
        double cost = 0;
        for (int i = 0; i < num_nodes; i++)
        {
            num_lanes = tld[i].length;
            for (int j = 0; j < num_lanes; j++)
            {
                if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                    cost += tld[i][j].getTL().getLane()
                            .getNumRoadusersWaiting();
            }
        }
    }

    /**
     * Calculates how every traffic light should be switched Per node, per sign
     * the waiting roadusers are passed and per each roaduser the gain is
     * calculated.
     * 
     * @param The
     *            TLDecision is a tuple consisting of a traffic light and a
     *            reward (Q) value, for it to be green
     * @see gld.algo.tlc.TLDecision
     */
    public TLDecision[][] decideTLs()
    {
        updateCost();

        setPathwiseDelays();        

        updateNumElapsedCycles();
        clearNumRedCycles();

        int num_dec;
        int gain = 0;

        periodIndex++;
        if (periodIndex == switchPeriod)
        {
            periodIndex = 0;
            for (int i = 0; i < junctions.length; i++)
            {
                int configIndex = (junctions[i].getConfigIndex() + 1)
                        % junctions[i].getSignConfigs().length;
                junctions[i].setConfigIndex(configIndex);
            }

        }

        Sign[][] chosenSCList = new Sign[junctions.length][];
        Sign[][] sc;

        // Action that is for all junctions...
        for (int j = 0; j < junctions.length; j++)
        {
            // Copy this junction's chosen action
            sc = junctions[j].getSignConfigs();
            chosenSCList[j] = sc[junctions[j].getConfigIndex()];
        }

        // For all Nodes
        Sign[] chosenSC = null;
        for (int i = 0; i < num_nodes; i++)
        {
            for (int j2 = 0; j2 < junctions.length; j2++)
            {
                if (junctions[j2].getId() == nodes[i].getId())
                {
                    chosenSC = chosenSCList[j2];
                }
            }

            num_dec = tld[i].length;
            // System.out.println("Node " + i + " Length: " + num_dec);
            // For all Trafficlights
            for (int j = 0; j < num_dec; j++)
            {
                tld[i][j].setGain(0);
            }
            for (int j = 0; j < num_dec; j++)
            {
                // if(tld[i][j].getTL().getNode().getType() == Node.JUNCTION)
                if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                {
                    for (int k = 0; k < chosenSC.length; k++)
                    {
                        if (tld[i][j].getTL().getLane().getId() == chosenSC[k]
                                .getLane().getId())
                        {
                            gain = 1;
                            tld[i][j].setGain(gain);
                        }
                    }
                }
                else
                {
                    tld[i][j].setGain(0);
                }
            }
        }

        return tld;
    }

    public void setPathwiseDelays()
    {
//        delaysPathWise.clear();
        for (int i = 0; i < num_nodes; i++)
        {
            int num_dec = tld[i].length;
            for (int j = 0; j < num_dec; j++)
            {
                if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                {
                    LinkedList queue = tld[i][j].getTL().getLane().getQueue();
                    ListIterator li = queue.listIterator();
                    Roaduser r = null;
                    while (li.hasNext())
                    {
                        r = (Roaduser) li.next();

                        ArrayList<Double> darr = delaysPathWise.get(new Key(
                                r.getStartNode().getId(), r.getDestNode()
                                        .getId()));
                        if (darr == null)
                        {
                            darr = new ArrayList<Double>();
                        }
                        if (r.getDelay() > 0)
                        {
                            darr.add((double) r.getDelay());
                            delaysPathWise.put(new Key(r.getStartNode()
                                    .getId(), r.getDestNode().getId()), darr);
                        }
                    }
                }
            }
        }
    }


    /** sets kappa of tld to 0 if trafficlight was switched green last cycle */
    public void updateNumElapsedCycles()
    {

        TLDecision curDec;
        int nRedCycles = 0;
        for (int i = 0; i < tld.length; i++)
        { // for all nodes
          // System.out.println("node " + i);
            for (int j = 0; j < tld[i].length; j++)
            { // for all inbound lanes in node
                curDec = tld[i][j];
                if (!tld[i][j].getTL().getState())
                    curDec.addNumRedCycles(1);
                nRedCycles += curDec.getNumRedCycles();

            }
        }
    }

    public void clearNumRedCycles()
    {
        for (int i = 0; i < tld.length; i++)
        { // for all nodes
            for (int j = 0; j < tld[i].length; j++)
            { // for all inbound lanes in node
                if (tld[i][j].getTL().getCycleSwitchedToGreen() == getCurCycle() - 1)
                {
                    tld[i][j].setNumRedCycles(0);
                }
            }
        }
    }

    public void updateRoaduserMove(Roaduser _ru, Drivelane _prevlane,
            Sign _prevsign, int _prevpos, Drivelane _dlanenow, Sign _signnow,
            int _posnow, PosMov[] posMovs, Drivelane desired)
    { // No needed
    }

    // XMLSerializable implementation

    public XMLElement saveSelf() throws XMLCannotSaveException
    {
        XMLElement result = super.saveSelf();
        result.setName(shortXMLName);
        return result;
    }

    public String getXMLName()
    {
        return "model." + shortXMLName;
    }

}
