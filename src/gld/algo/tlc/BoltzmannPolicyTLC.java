package gld.algo.tlc;

import gld.*;
import gld.sim.*;
import gld.algo.tlc.*;
import gld.infra.*;
import gld.utils.*;
import gld.xml.*;

import java.io.IOException;
import java.util.*;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Simulates traffic for a given Boltzmann policy paramter and collects the
 * delays for each traveler
 * 
 * @version 0.1 11 Feb 2016
 * @author Prashanth L.A.
 * @see gld.tester.CptRunner
 */
public class BoltzmannPolicyTLC extends TLCBase
{
    public final static String shortXMLName = "RunWithFixedPolicy";
    public static boolean first = true;
    public static boolean first_time_compute = true;

    double policyDivisor; // sum eQ
    ListCombinationGenerator<Integer> actionList;

    double theta[];

    int actionDim = 0;
    int[] scIndex;

    int[] selectedAction; // the red-greenconfig selected last

    int nRu;
    /**
     * The constructor for TL controllers
     */
    public BoltzmannPolicyTLC(Infrastructure i)
    {
        super(i);
        nRu = 1;
    }

    /**
     * Set the Boltzmann policy parameter
     */
    public void setTheta(double[] arr)
    {
        System.arraycopy(arr, 0, theta, 0, theta.length);
    }

    /**
     * Initialize the TLC algorithm
     */
    void initialize(Infrastructure infra)
    {
        nodes = infra.getAllNodes();
        junctions = infra.getJunctions();
        num_nodes = nodes.length;

        for (int i = 0; i < nodes.length; i++)
        {
            numRealSigns += nodes[i].getNumSigns();
        }
        dimStateFeatures = numRealSigns;

        // Create feature vectors
        currentFeatures = new double[dimStateFeatures];
        oldFeatures = new double[dimStateFeatures];

        // Boltzmann policy parameter
        theta = new double[dimStateFeatures];

        totalReward = 0;
        gamma = 0.9;

        queueLengths = new int[dimStateFeatures];
        elapsedTimes = new int[dimStateFeatures];
        selectedAction = new int[junctions.length];

        // contains current signconfig as an index to junctions.actionIndices
        scIndex = new int[junctions.length];

        delaysPathWise = new HashMap<Key, ArrayList<Double>>();

        System.out.println("Boltzmann policy TLC initialized");
    }

    /**
     * Initialize the set of actions (red-green combinations) across junctions
     * for the entire road network
     */
    public void loadActionList(Infrastructure infra)
    {
        int actionIndices[][];
        List<List<Integer>> ll = new ArrayList<List<Integer>>();

        for (int i = 0; i < junctions.length; i++)
        {
            int numSignConfigs = junctions[i].getSignConfigs().length;
            actionIndices = new int[numSignConfigs][numSignConfigs];

            List<Integer> l1 = new ArrayList<Integer>();

            for (int j = 0; j < actionIndices.length; j++)
            {
                l1.add(new Integer(j));
                for (int k = 0; k < actionIndices[j].length; k++)
                {
                    actionIndices[j][k] = 0;
                }
                actionIndices[j][j] = 1;
            }
            junctions[i].setActionIndices(actionIndices);
            ll.add(l1);
        }
        actionList = new ListCombinationGenerator(ll);
    }

    public void setInfrastructure(Infrastructure infra)
    {
        super.setInfrastructure(infra);
        try
        {
            initialize(infra);
            loadActionList(infra);
        }
        catch (Exception e)
        {
            System.out.println("RunWithFixedPolicy failed to initialise.");
        }
    }

    /**
     * Return state action features for the current state and a given action
     */
    public double[] loadStateActionFeatures(int[] action)
    {
        double stateActionFeatureVector[] = new double[dimStateFeatures];

        int[] redgreenlist = translateActionindexToRedGreenList(action);

        for (int i = 0; i < queueLengths.length; i++)
        {
            // Red case
            if (queueLengths[i] < L1 && elapsedTimes[i] < T1
                    && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.01;
            }
            if (queueLengths[i] < L1 && elapsedTimes[i] >= T1
                    && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.2;
            }
            if (queueLengths[i] >= L1 && queueLengths[i] < L2
                    && elapsedTimes[i] < T1 && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.3;
            }
            if (queueLengths[i] >= L1 && queueLengths[i] < L2
                    && elapsedTimes[i] >= T1 && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.4;
            }
            if (queueLengths[i] > L2 && elapsedTimes[i] < T1
                    && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.5;
            }
            if (queueLengths[i] > L2 && elapsedTimes[i] >= T1
                    && redgreenlist[i] == RED)
            {
                stateActionFeatureVector[i] = 0.6;
            }

            // Green case
            if (queueLengths[i] < L1 && elapsedTimes[i] < T1
                    && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.6;
            }
            if (queueLengths[i] < L1 && elapsedTimes[i] >= T1
                    && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.5;
            }
            if (queueLengths[i] >= L1 && queueLengths[i] < L2
                    && elapsedTimes[i] < T1 && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.4;
            }
            if (queueLengths[i] >= L1 && queueLengths[i] < L2
                    && elapsedTimes[i] >= T1 && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.3;
            }
            if (queueLengths[i] > L2 && elapsedTimes[i] < T1
                    && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.2;
            }
            if (queueLengths[i] > L2 && elapsedTimes[i] >= T1
                    && redgreenlist[i] == GREEN)
            {
                stateActionFeatureVector[i] = 0.01;
            }
        }

        return stateActionFeatureVector;
    }

    /**
     * Calculate policy divisor - the denominator in the Boltzmann policy
     * formula
     */
    private void calcPolicyDivisor()
    {
        policyDivisor = 0;
        int[] selectedAction = new int[junctions.length];
        int[] currAction = new int[junctions.length];
        while (actionList.hasMoreCombinations())
        {
            List<Integer> currentActionTuple = actionList.getNextCombination();
            for (int i = 0; i < selectedAction.length; i++)
            {
                currAction[i] = (int) currentActionTuple.get(i);
            }

            double[] currPhiSa = loadStateActionFeatures(currAction);
            double result = 0;
            for (int i = 0; i < currPhiSa.length; i++)
            {
                result += theta[i] * currPhiSa[i];
            }
            policyDivisor += Math.exp(result);
        }
        actionList.setHasMoreCombinations(true);
    }

    /**
     * Choose action using Boltzmann policy distribution
     */
    int[] choose_action()
    {
        double rndValue;
        double offset;

        rndValue = Math.random();
        offset = 0;

        calcPolicyDivisor();

        int[] currAction = new int[junctions.length];

        while (actionList.hasMoreCombinations())
        {
            List<Integer> currentActionTuple = actionList.getNextCombination();

            for (int i = 0; i < selectedAction.length; i++)
            {
                currAction[i] = (int) currentActionTuple.get(i);
            }

            double[] currPhiSa = loadStateActionFeatures(currAction);
            double result = 0;
            for (int i = 0; i < currPhiSa.length; i++)
            {
                result += theta[i] * currPhiSa[i];
            }

            double probOfThisCombination = Math.exp(result) / policyDivisor;

            if (rndValue > offset && rndValue < offset + probOfThisCombination)
            {
                System.arraycopy(currAction, 0, selectedAction, 0,
                        selectedAction.length);
            }
            offset += probOfThisCombination;
        }
        actionList.setHasMoreCombinations(true);

        return selectedAction;
    }

    /**
     * Converted the selected action into a sign configuration
     */
    private Sign[][] selectSignConfig()
    {
        selectedAction = choose_action();

        Sign[][] chosenSCList = new Sign[junctions.length][];
        Sign[][] sc;

        try
        {
            for (int j = 0; j < junctions.length; j++)
            {
                scIndex[j] = selectedAction[j];
                sc = junctions[j].getSignConfigs();
                chosenSCList[j] = sc[scIndex[j]];
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return chosenSCList;
    }

    /**
     * Calculates how every traffic light should be switched Per node, per sign
     * the waiting roadusers are passed and per each roaduser the gain is
     * calculated.
     * 
     * @param The
     *            TLDecision is a tuple consisting of a traffic light and a
     *            reward (Q) value, for it to be green
     * @see gld.algo.tlc.TLDecision
     */
    public TLDecision[][] decideTLs()
    {
        try
        {
            updateNumElapsedCycles();

            int num_lanes;
            int pos = 0;
            for (int i = 0; i < num_nodes; i++)
            {
                num_lanes = tld[i].length;
                for (int j = 0; j < num_lanes; j++)
                {
                    if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                    {
                        queueLengths[pos] = tld[i][j].getTL().getLane()
                                .getNumRoadusersWaiting();
                        elapsedTimes[pos] = tld[i][j].getNumRedCycles();
                        pos++;
                    }
                }
            }

            Sign[][] chosenSignConfigs = selectSignConfig();

            clearNumGreenCycles();
            clearNumRedCycles();

            // Force GLD to take the selected action (from choose_action()), by
            // setting the
            // gains in the tld matrix appropriately
            Sign[] chosenSC = null;
            for (int i = 0; i < num_nodes; i++)
            {
                for (int j2 = 0; j2 < junctions.length; j2++)
                {
                    if (junctions[j2].getId() == nodes[i].getId())
                    {
                        chosenSC = chosenSignConfigs[j2];
                    }
                }

                int gain;
                int num_dec = tld[i].length;
                // For all Trafficlights
                for (int j = 0; j < num_dec; j++)
                {
                    tld[i][j].setGain(0);
                }

                for (int j = 0; j < num_dec; j++)
                {
                    if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                    {
                        for (int k = 0; k < chosenSC.length; k++)
                        {
                            if (tld[i][j].getTL().getLane()
                                    .getId() == chosenSC[k].getLane().getId())
                            {
                                gain = 1;
                                tld[i][j].setGain(gain);
                            }
                        }
                        // if (tld[i][j].getTL().getLane().isFull())
                        // {
                        // gain = 10;
                        // tld[i][j].setGain(gain);
                        // }
                    }
                    else
                    {
                        tld[i][j].setGain(0);
                    }
                }
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int curCycle = getCurCycle();
        if (curCycle == getSimulationDuration() - 1)
        {
            System.out.println("Cycle: " + curCycle + " simDur: "
                    + getSimulationDuration());
            setPathwiseDelays();
        }

        return tld;
    }

    public void setPathwiseDelays()
    {
        for (int i = 0; i < num_nodes; i++)
        {
            int num_dec = tld[i].length;
            for (int j = 0; j < num_dec; j++)
            {
                if (tld[i][j].getTL().getType() == Sign.TRAFFICLIGHT)
                {
                    LinkedList queue = tld[i][j].getTL().getLane().getQueue();
                    ListIterator li = queue.listIterator();
                    Roaduser r = null;
                    while (li.hasNext())
                    {
                        r = (Roaduser) li.next();

                        ArrayList<Double> darr = delaysPathWise
                                .get(new Key(r.getStartNode().getId(),
                                        r.getDestNode().getId()));
                        if (darr == null)
                        {
                            darr = new ArrayList<Double>();
                        }
                        if (r.getDelay() > 0)
                        {
                            Key currKey = new Key(r.getStartNode().getId(),
                                    r.getDestNode().getId());
                            Double dval = (double) r.getDelay();
                            darr.add(dval);
                            delaysPathWise.put(currKey, darr);
                        }
                    }
                }
            }
        }
    }

    public void printPathwiseDelays()
    {
        System.out.println("Pathwise Delays:");
        for (Key c : delaysPathWise.keySet())
        {
            System.out.print("Key: " + Arrays.toString(c.getVals()));
            ArrayList<Double> a = delaysPathWise.get(c);
            for (int i = 0; i < a.size(); i++)
            {
                System.out.print((Double) a.get(i) + ",");
            }
            System.out.println();
        }
    }

    public void updateDelay(Roaduser _ru)
    {
//        System.out.println("Here" + nRu + " " + _ru.getDelay());
//        nRu++;
        ArrayList<Double> darr = delaysPathWise.get(new Key(
                _ru.getStartNode().getId(), _ru.getDestNode()
                        .getId()));
        if (darr == null)
        {
            darr = new ArrayList<Double>();
        }
        
        if (_ru.getDelay() > 0)
        {
            darr.add((double) _ru.getDelay());
//            System.out.println("darr: " + darr.toString());
            delaysPathWise.put(new Key(_ru.getStartNode()
                    .getId(), _ru.getDestNode().getId()), darr);
        }
    }

    public void updateRoaduserMove(Roaduser _ru, Drivelane _prevlane,
            Sign _prevsign, int _prevpos, Drivelane _dlanenow, Sign _signnow,
            int _posnow, PosMov[] posMovs, Drivelane desired)
    {
    }

    // XMLSerializable implementation
    public XMLElement saveSelf() throws XMLCannotSaveException
    {
        XMLElement result = super.saveSelf();
        result.setName(shortXMLName);
        return result;
    }

    public String getXMLName()
    {
        return "model." + shortXMLName;
    }
}
