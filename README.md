# README #

### What is this repository for? ###

This software package (in Java) provides the implementation of a reinforcement learning (RL) based traffic light control algorithm that incorporates a Cumulative Prospect Theory (CPT) based performance criterion. The CPT-RL TLC algorithm uses simultaneous perturbation stochastic approximation (SPSA) for finding the best policy for switching traffic lights so that a CPT-based performance criterion is maximized. See http://jmlr.org/proceedings/papers/v48/la16.pdf for a detailed description of CPT+RL and its application to traffic
signal control.

### Notes on usage ###

This package is based on the source code of the Green Light District (GLD) traffic simulator (https://sourceforge.net/projects/stoplicht/). GLD codebase is modified to include the RL based TLC algorithm.
The files relevant to CPT+RL based TLC in the distribution are:

i) gld.tester.CptRunner.java --> Wrapper for running the CPT policy optimizer

ii) gld.tester.CptSpsaOuterLoop.java --> Implementation of the CPT policy optimizing outer loop that is based on SPSA

iii) gld.algo.tlc.RunWithFixedPolicy --> Simulates traffic for a given Boltzmann policy paramter and collects the delays for each traveler

On input parameters for CPT+RL based TLC (to be set in the main function of CptRunner.java): 

i) numIterationsForPolicyTuning -> the number of iterations of the SPSA outer loop

ii) numIterationsForTesting -> After the policy is tuned using SPSA, the policy parameter is fixed and then a number of independent simulations (that this variable is set to) are run and the CPT-value from each simulation is recorded

iii) simulatedMDPTrajectoryLength -> length of each simulated trajectory

In addition, the weightType for both optimizeCPTpolicy and testPolicy have to be set to either "CPT" or "EUT" or "AVG", based on whether the performance criterion is CPT (uses utility functions to treat gains/losses separately and weights to distort probabilities) or EUT (uses utilities for gain/loss separately, but no probability distortions) or AVG (regular expectation objective where gains/losses are handled in the same manner and no probability distortions are incorporated).

### Who do I talk to? ###

* Prashanth L.A. 
email: prashla@cse.iitm.ac.in